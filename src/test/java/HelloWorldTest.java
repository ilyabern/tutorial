import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 4/6/13
 * Time: 1:56 PM
 */
public class HelloWorldTest {

    @Test
    public void testAdd() {
        assertEquals(3, HelloWorld.add(2, 1));
    }

    @Test
    public void testSubtract() {
        assertEquals(10, HelloWorld.subtract(15, 5));
    }

    @Test
    public void testMultiply() {
        assertEquals(50, HelloWorld.multiply(10, 5));
    }
}
