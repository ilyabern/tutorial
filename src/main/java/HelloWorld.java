/**
 * Created with IntelliJ IDEA.
 * User: ilya
 * Date: 4/6/13
 * Time: 1:43 PM
 */
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello world!");
    }


    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }
}
